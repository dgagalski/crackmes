#include <stdio.h>
#include <string.h>
#include <sys/ptrace.h>

void encrypt(char *s)
{
	int ix;

	for (ix = 0; ix < strlen(s); ix++)
		s[ix] -= 10;
}

int main()
{
	if (ptrace(PTRACE_TRACEME, 0, NULL, NULL)) {
		puts("Debugger Detected");
		return 1;
	}

	char pass[64] = "9J<q=^':h*#*d:#+Jh*9)#'+#>)'Bs";
	char input[64];

	printf("Enter a Password: ");
	scanf("%s", input);
	encrypt(input);

	if (!strcmp(input, pass))
		printf("Congrats, Access Granted!!");
	else
		printf("Try Harder and Debug More!");


	return 0;
}
