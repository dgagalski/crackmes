#include <stdio.h>
#include <string.h>

int main(int argc, char **argv)
{
	if (argc != 2) {
		printf("format: ./crackme STRING\n");
		return 1;
	}

	char *key = "secret";	/* var_18 */
	long khash = 0;		/* var_20 */
	for (int kix = 0; strlen(key) > kix; kix++) /* var_28 */
		khash += key[kix] ^ kix;

	char *input = argv[1];	/* var_30 */
	if (!strcmp(input, key)) {
		printf("sorry...\n");
		return 1;
	}

	long ihash = 0;		/* var_38 */
	for (int iix = 0; strlen(input) > iix; iix++)	/* var_40 */
		ihash += input[iix] ^ iix;

	if (khash == ihash) {
		printf("you did it!\n");
		return 0;
	} else {
		printf("sorry...\n");
		return 1;
	}

}
