## crackmes.one reversing practice

| name | author | difficulty | quality | platform | arch | reverse date |
| :--: | :----: | :--------: | :-----: | :------: | :--: | :----------: |
| 64e91b92d931496abf9091e3 | ithawk | 1.5 | 3.0 | UNIX | x86-64 | 1/09/23 |
| 64bf4185fc4ca2e6ca3d0f25 | 0xdecaf | 2.0 | 4.0 | UNIX | x86-64 | 1/09/23 |
| 64f1f7afd931496abf909525 | s4r | 3.0 | 4.0 | UNIX | x86-64 | 5/09/23 |
| 6420a9cd33c5d447bc761ddc | yariza | 2.5 | 3.0 | UNIX | x86-64 | 7/09/23 |
| 63c4ee1a33c5d43ab4ecf49a | skibur | 1.0 | 1.5 | UNIX | x86-64 | 9/09/23 |
