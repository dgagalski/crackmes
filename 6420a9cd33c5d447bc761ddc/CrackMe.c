#include <time.h>
#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <openssl/sha.h>

char *generate_license_key(char *a1, long a2)
{
	char *x;
	unsigned char data[256];
	unsigned char md[SHA256_DIGEST_LENGTH];
	int len, i;
	SHA256_CTX ctx;

	x = malloc(16);

	len = snprintf(data, 256, "%s:%s:%ld", "PREMIUM", a1, a2);

	SHA256_Init(&ctx);
	SHA256_Update(&ctx, data, l);
	SHA256_Final(md, &ctx);

	for (i=0; i<16; i++) /* convert first 16B into [A-Z] */
		x[i] = (md[i] % 26) + 65;

	return x;
}

int main(void)
{
	char in[16];
	char x[] = "Butters Stotch";
	long r;
	char *k;

	r = time(NULL) + 0x1e13380;

	k = generate_license_key(x, r);

	puts("\n* Welcome to the worlds first uncrackable program! *\n");
	printf("[*] Enter your license: ");

	scanf("%s", in);

	if (!strcmp(in, k)) {
		printf("Well done, flag: %s", k);
	} else if (strcmp(in, k)) {
		fwrite("[-] Wrong license.\n", 1, 19, stderr);
	}

	free(k);
}
