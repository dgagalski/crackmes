#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/types.h>
#include <pthread.h>
#include <malloc.h>
#include <ncurses.h>

unsigned long next = 1;
int current_fruit_x = 0;
int current_fruit_y = 0;
int game_result = -34;
unsigned char key = 'D';
pthread_mutex_t map_lock;
char game_map[20][40];
WINDOW *win;

int password_idx = 0;
char password[32], chacha_nonce[12];
char encrypted_flag[29] = {
	0xf4, 0xb8, 0xc0, 0x90, 0xcd, 0xf6, 0xcb, 0xf7,
	0xc5, 0x42, 0x30, 0xf3, 0x75, 0x04, 0x2c, 0x4b,
	0xfd, 0x9d, 0x96, 0x02, 0x57, 0x79, 0x9a, 0x7a,
	0xee, 0x75, 0xd9, 0xf0, 0x4c
};

struct blk {
	u_int32_t gb[16];	/* 0x00 - 0x3c (generated block) */
	u_int64_t rcnt;		/* 0x40        (round counter) */
	u_int32_t pass[8];	/* 0x48 - 0x64 */
	u_int32_t nonce[3];	/* 0x68 - 0x70 */
	u_int64_t bcnt;		/* 0x78        (block counter) */
	u_int32_t kb[16];	/* 0x80 - 0xbc (secret key block) */
};

struct part {
	int x;
	int y;
	int dir;	/* direction */
};

struct snake {
	int id;		/* color */
	int length;	/* 3 - 550 */
	struct part p[800];
};

unsigned int rotl32(unsigned int x, unsigned int r) {
	__asm__(
		"rol %%cl, %0"
		: "=r" (x) : "c" (r), "0"(x)
	);
	return x;
}

void srand(unsigned int x)
{
	next = x;
}

int rand(void)
{
	next = next * 0x41c64e6d + 12345;

	return (next >> 16) & 0x7fff;
}

unsigned int pack4(void *s)
{
	unsigned int ret = 0;

	ret |= *(u_int8_t *)s;
	ret |= *(u_int8_t *)(s+1) << 8;
	ret |= *(u_int8_t *)(s+2) << 16;
	ret |= *(u_int8_t *)(s+3) << 24;

	return ret;
}

void chacha20_init_block(struct blk *b, char *pass, char *nonce)
{
	/*
	 * [c][c][c][c] constant
	 * [k][k][k][k] key
	 * [k][k][k][k]
	 * [C][n][n][n] counter + number used once (nonce)
	 */

	memcpy(b->pass, pass, 32);
	memcpy(b->nonce, nonce, 12);

	char *c = "expand 32-byte k";
	b->kb[0] = pack4(c);
	b->kb[1] = pack4(c+4);
	b->kb[2] = pack4(c+8);
	b->kb[3] = pack4(c+12);

	b->kb[4] = pack4(pass);
	b->kb[5] = pack4(pass+4);
	b->kb[6] = pack4(pass+8);
	b->kb[7] = pack4(pass+12);
	b->kb[8] = pack4(pass+16);
	b->kb[9] = pack4(pass+20);
	b->kb[10] = pack4(pass+24);
	b->kb[11] = pack4(pass+28);

	b->kb[12] = 0;
	b->kb[13] = pack4(nonce);
	b->kb[14] = pack4(nonce+4);
	b->kb[15] = pack4(nonce+8);

	memcpy(b->nonce, nonce, 12);
}

void chacha20_block_set_counter(struct blk *b, unsigned long counter)
{
	b->kb[12] = (unsigned int)counter;
	b->kb[13] = pack4(b->nonce) + (counter >> 32);
}

void chacha20_init_context(struct blk *b, char *pass,
		char *nonce, unsigned long counter)
{
	memset(b, 0, sizeof(struct blk)); /* 192 */

	chacha20_init_block(b, pass, nonce);
	chacha20_block_set_counter(b, counter);

	/*
	 * how much bytes skip for decryption
	 */
	b->bcnt = counter;
	/*
	 * immediatelly start keygen algorithm
	 * don't XOR with secret key
	 */
	b->rcnt = 64;
}

void chacha20_block_next(struct blk *b)
{
	unsigned int *count;
	int i, j, k;

	for (i=0; i<16; i++)
		b->gb[i] = b->kb[i];

	/* 10 iterations per 2 rounds (chacha20/20) */
	for (j=0; j<10; j++) {
		/*   column 1	(1/4 round)
		 * [a][ ][ ][ ] +0
		 * [b][ ][ ][ ] +4
		 * [c][ ][ ][ ] +8
		 * [d][ ][ ][ ] +12
		 */
		b->gb[0]  += b->gb[4];	/* a+=b; d^=a; d<<<=16 */
		b->gb[12] = rotl32(b->gb[12] ^ b->gb[0], 16);

		b->gb[8]  += b->gb[12]; /* c+=d; b^=c; b<<<=12 */
		b->gb[4]  = rotl32(b->gb[4] ^ b->gb[8], 12);

		b->gb[0]  += b->gb[4];	/* a+=b; d^=a; d<<<=8  */
		b->gb[12] = rotl32(b->gb[12] ^ b->gb[0], 8);

		b->gb[8]  += b->gb[12];	/* c+=d; b^=c; b<<<=7  */
		b->gb[4]  = rotl32(b->gb[4] ^ b->gb[8], 7);

		/* column 2
		 * [ ][a][ ][ ] +1
		 * [ ][b][ ][ ] +5
		 * [ ][c][ ][ ] +9
		 * [ ][d][ ][ ] +13
		 */
		b->gb[1]  += b->gb[5];
		b->gb[13] = rotl32(b->gb[13] ^ b->gb[1], 16);

		b->gb[9]  += b->gb[13];
		b->gb[5]  = rotl32(b->gb[5] ^ b->gb[9], 12);

		b->gb[1]  += b->gb[5];
		b->gb[13] = rotl32(b->gb[13] ^ b->gb[1], 8);

		b->gb[9]  += b->gb[13];
		b->gb[5]  = rotl32(b->gb[5] ^ b->gb[9], 7);

		/* column 3
		 * [ ][ ][a][ ] +2
		 * [ ][ ][b][ ] +6
		 * [ ][ ][c][ ] +10
		 * [ ][ ][d][ ] +14
		 */
		b->gb[2]  += b->gb[6];
		b->gb[14] = rotl32(b->gb[14] ^ b->gb[2], 16);

		b->gb[10] += b->gb[14];
		b->gb[6]  = rotl32(b->gb[6] ^ b->gb[10], 12);

		b->gb[2]  += b->gb[6];
		b->gb[14]  = rotl32(b->gb[14] ^ b->gb[2], 8);

		b->gb[10] += b->gb[14];
		b->gb[6]  = rotl32(b->gb[6] ^ b->gb[10], 7);

		/* column 4
		 * [ ][ ][ ][a] +3
		 * [ ][ ][ ][b] +7
		 * [ ][ ][ ][c] +11
		 * [ ][ ][ ][d] +15
		 */
		b->gb[3]  += b->gb[7];
		b->gb[15] = rotl32(b->gb[15] ^ b->gb[3], 16);

		b->gb[11] += b->gb[15];
		b->gb[7]  = rotl32(b->gb[7] ^ b->gb[11], 12);

		b->gb[3]  += b->gb[7];
		b->gb[15] = rotl32(b->gb[15] ^ b->gb[3], 8);

		b->gb[11] += b->gb[15];
		b->gb[7]  = rotl32(b->gb[7] ^ b->gb[11], 7);


		/* diagonal 1
		 * [a][ ][ ][ ] +0
		 * [ ][b][ ][ ] +5
		 * [ ][ ][c][ ] +10
		 * [ ][ ][ ][d] +15
		 */
		b->gb[0]  += b->gb[5];
		b->gb[15] = rotl32(b->gb[15] ^ b->gb[0], 16);

		b->gb[10] += b->gb[15];
		b->gb[5]  = rotl32(b->gb[5] ^ b->gb[10], 12);

		b->gb[0]  += b->gb[5];
		b->gb[15] = rotl32(b->gb[15] ^ b->gb[0], 8);

		b->gb[10] += b->gb[15];
		b->gb[5]  = rotl32(b->gb[5] ^ b->gb[10], 7);

		/* diagonal 2
		 * [ ][a][ ][ ] +1
		 * [ ][ ][b][ ] +6
		 * [ ][ ][ ][c] +11
		 * [d][ ][ ][ ] +12
		 */
		b->gb[1]  += b->gb[6];
		b->gb[12] = rotl32(b->gb[12] ^ b->gb[1], 16);

		b->gb[11] += b->gb[12];
		b->gb[6]  = rotl32(b->gb[6] ^ b->gb[11], 12);

		b->gb[1]  += b->gb[6];
		b->gb[12] = rotl32(b->gb[12] ^ b->gb[1], 8);

		b->gb[11] += b->gb[12];
		b->gb[6]  = rotl32(b->gb[6] ^ b->gb[11], 7);

		/* diagonal 3
		 * [ ][ ][a][ ] +2
		 * [ ][ ][ ][b] +7
		 * [c][ ][ ][ ] +8
		 * [ ][d][ ][ ] +13
		 */
		b->gb[2]  += b->gb[7];
		b->gb[13] = rotl32(b->gb[13] ^ b->gb[2], 16);

		b->gb[8]  += b->gb[13];
		b->gb[7]  = rotl32(b->gb[7] ^ b->gb[8], 12);

		b->gb[2]  += b->gb[7];
		b->gb[13] = rotl32(b->gb[13] ^ b->gb[2], 8);

		b->gb[8]  += b->gb[13];
		b->gb[7]  = rotl32(b->gb[7] ^ b->gb[8], 7);

		/* diagonal 4
		 * [ ][ ][ ][a] +3
		 * [b][ ][ ][ ] +4
		 * [ ][c][ ][ ] +9
		 * [ ][ ][d][ ] +14
		 */
		b->gb[3]  += b->gb[4];
		b->gb[14] = rotl32(b->gb[14] ^ b->gb[3], 16);

		b->gb[9]  += b->gb[14];
		b->gb[4]  = rotl32(b->gb[4] ^ b->gb[9], 12);

		b->gb[3]  += b->gb[4];
		b->gb[14] = rotl32(b->gb[14] ^ b->gb[3], 8);

		b->gb[9] += b->gb[14];
		b->gb[4] ^= b->gb[9];
		b->gb[4]  = rotl32(b->gb[4], 7);
	}

	for (k=0; k<16; k++)
		b->gb[k] += b->kb[k];

	count = b->kb + 12;
	count[0]++;
	if (count[0] == 0) {
		count[1]++;
		if (count[1] == 0)
			_exit(1);
	}
}

void chacha20_xor(struct blk *b, void *data, unsigned long length)
{
	unsigned char *ks = (unsigned char *)b->gb;
	long i;

	for (i=0; i<length; i++) {
		if (b->rcnt > 63) {
			chacha20_block_next(b);
			b->rcnt = 0;
		}

		*(unsigned char *)(data + i) ^= ks[b->rcnt];
		b->rcnt++;
	}
}

struct snake *make_snake(int id, int y, int x)
{
	struct snake *s;

	pthread_mutex_lock(&map_lock);

	game_map[y][x] = -id;
	game_map[y+1][x] = game_map[y+2][x] = id;

	pthread_mutex_unlock(&map_lock);

	s = (struct snake *)malloc(sizeof(struct snake));

	s->id = id;
	s->length = 3;

	s->p[0].y = y;
	s->p[0].x = x;
	s->p[0].dir = 'W';

	s->p[1].y = y+1;
	s->p[1].x = x;
	s->p[1].dir = 'W';

	s->p[799].y = y+2;
	s->p[799].x = x;
	s->p[799].dir = 'W';

	return s;
}

void kill_snake(struct snake *s)
{
	int i;

	pthread_mutex_lock(&map_lock);

	game_map[s->p[799].y][s->p[799].x] = game_map[s->p[0].y][s->p[0].x] = 0;
	for (i=0; i < s->length+2; i++)
		game_map[s->p[i+1].y][s->p[i+1].x] = 0;

	pthread_mutex_unlock(&map_lock);

	free(s);
}

void add_fruit(void)
{
	int y, x;

	y = (rand() % 18) + 1;
	x = (rand() % 38) + 1;

	current_fruit_x = x;
	current_fruit_y = y;

	pthread_mutex_lock(&map_lock);

	game_map[y][x] = 50;

	pthread_mutex_unlock(&map_lock);
}

void eat_fruit(struct snake *s, char c)
{
	password[password_idx] += current_fruit_x;
	password[password_idx] *= current_fruit_y;

	password_idx = (password_idx+1) % 32;

	memmove(&s->p[2], &s->p[1], (s->length - 2) * 12);

	s->p[1].y = s->p[0].y;
	s->p[1].x = s->p[0].x;
	s->p[1].dir = s->p[0].dir;

	switch (c) {
		case 'W': /* 0x57 */
			s->p[0].y--;
			s->p[0].dir = 'W';

			if (game_map[s->p[0].y][s->p[0].x+1] != 0x32)
				break;

			pthread_mutex_lock(&map_lock);
			game_map[s->p[0].y][s->p[0].x+1] = 0;
			pthread_mutex_unlock(&map_lock);

			break;
		case 'S': /* 0x53 */
			s->p[0].y++;
			s->p[0].dir = 'S';

			if (game_map[s->p[0].y][s->p[0].x+1] != 0x32)
				break;

			pthread_mutex_lock(&map_lock);
			game_map[s->p[0].y][s->p[0].x+1] = 0;
			pthread_mutex_unlock(&map_lock);

			break;
		case 'A': /* 0x41 */
			s->p[0].x--;
			s->p[0].dir = 'A';
			break;
		case 'D': /* 0x44 */
			s->p[0].x++;
			s->p[0].dir = 'D';
			break;
	}

	pthread_mutex_lock(&map_lock);

	game_map[s->p[0].y][s->p[0].x] = -s->id;
	game_map[s->p[1].y][s->p[1].x] = s->id;

	pthread_mutex_unlock(&map_lock);

	s->length++;
}

void *get_input(void *arg)
{
	unsigned char c;

	while (game_result == -34) {
		c = 0;

		c = wgetch(stdscr);
		c = toupper((char)c);

		if (c == '.') {
			game_result = -30;
			break;
		} else if (c == 'W' || c == 'S' || c == 'A' || c == 'D') {
			if (key != c)
				key = c;
		}
	}

	return NULL;
}

int make_thread(void *(*func)(void *), void *arg)
{
	pthread_attr_t attr;
	pthread_t t;
	int s;

	s = pthread_attr_init(&attr);
	if (s != 0)
		return s;

	s = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	if (s == 0)
		s = pthread_create(&t, &attr, func, arg);

	s = pthread_attr_destroy(&attr);

	return s;
}

int update_screen(void)
{
	struct blk b;
	WINDOW *fwin;
	int x, y, curr_pos, color, score;

	werase(stdscr);
	wborder(win, '+', '+', '+', '+', 0, 0, 0, 0);
	wrefresh(stdscr);
	wrefresh(win);

	score = 1;

	for (y=1; y<=18; y++) {
		for (x=1; x<=38; x++) {
			curr_pos = game_map[y][x];
			color = curr_pos % 7;

			wattr_on(stdscr, COLOR_PAIR(color), NULL);

			if (curr_pos > 0 && curr_pos != 50) {
				mvprintw(y, x, "  ");
				wattr_off(stdscr, COLOR_PAIR(color), NULL);
				score++;
			} else if (curr_pos < 0 && curr_pos != 50) {
				if (game_map[y-1][x] == -curr_pos)
					mvprintw(y, x, "..");
				else if (game_map[y+1][x] == -curr_pos)
					mvprintw(y, x, "**");
				else if (game_map[y][x-1] == -curr_pos)
					mvprintw(y, x, " :");
				else if (game_map[y][x+1] == -curr_pos)
					mvprintw(y, x, ": ");

				wattr_off(stdscr, COLOR_PAIR(color), NULL);
			}

			if (curr_pos == 50) {
				wattr_off(stdscr, COLOR_PAIR(color), NULL);
				mvprintw(y, x, "o");
			}
		}
	}

	char s1[34] = { 0 };
	char s2[34] = { 0 };

	snprintf(s1, 30, "score: %d", score);
	snprintf(s2, 30, "needed for flag: %d", 550);

	mvprintw(31, 5, s1);
	mvprintw(33, 5, s2);

	wrefresh(stdscr);

	if (game_result == -94) {
		int cnt = 0;
		chacha20_init_context(&b, password, chacha_nonce, cnt);
		chacha20_xor(&b, encrypted_flag, sizeof(encrypted_flag));
	
		if (encrypted_flag[0] != 'b' || encrypted_flag[1] != 'r' ||
	    	encrypted_flag[2] != 'b' || encrypted_flag[3] != '{')
			snprintf(encrypted_flag, sizeof(encrypted_flag),
			 	"%s", "No flag for you today...");
	
		fwin = newwin(7, 35, 5, 5);
		wborder(fwin, 0, 0, 0, 0, 0, 0, 0, 0);
		if (wmove(fwin, 2, 4) != -1)
			waddnstr(fwin, "Game Over - You Win!", -1);
		if (wmove(fwin, 3, 4) != -1)
			waddnstr(fwin, encrypted_flag, -1);
		if (wmove(fwin, 4, 4) != -1)
			waddnstr(fwin, "Press any key to quit.", -1);
	
		wbkgd(fwin, COLOR_PAIR(2));
		mvwin(fwin, 6, 2);
		wnoutrefresh(fwin);
		wrefresh(fwin);
		sleep(2);
		wgetch(fwin);
		delwin(fwin);
		werase(win);
		echo();
		curs_set(1);
		endwin();
	} else {
		return 0;
	}
}

void *gameplay(void *)
{
	struct snake *s;

	puts("Starting Gameplay!");
	srand(time(NULL) % 20000);
	srand(11292);

	int scolor = 2;

	s = make_snake(scolor, 5, 5);
	add_fruit();

	for (;;) {
		password[password_idx] += current_fruit_x;
		password[password_idx] *= current_fruit_y;
	
		password_idx = (password_idx + 1) % 32;
	
		s->length++;
	
		add_fruit();
	
		if (s->length >= 550)
			game_result = -94;
	
		update_screen();
	}
}

int main(int argc, char **argv)
{
	char pass[32] = {
		0x80 ,0x18 ,0x1c ,0x4a ,0xe0 ,0xfa ,0xc8 ,0xaa,
		0xf6 ,0x62 ,0x87 ,0x42 ,0x58 ,0x98 ,0xd7 ,0xc0,
		0xb0 ,0x74 ,0x78 ,0xc8 ,0x72 ,0x01 ,0x18 ,0x84,
		0x64 ,0x2e ,0xba ,0x41 ,0x3a ,0xaa ,0x52 ,0x20
	};

	initscr();
	cbreak();
	noecho();
	start_color();
	use_default_colors();
	curs_set(0);
	win = newwin(20, 40, 0, 0);

	init_pair(0, COLOR_WHITE, COLOR_BLUE); /* num, fg, bg */
	init_pair(1, COLOR_WHITE, COLOR_RED);
	init_pair(2, COLOR_WHITE, COLOR_GREEN);
	init_pair(3, COLOR_BLACK, COLOR_YELLOW);
	init_pair(4, COLOR_BLACK, COLOR_MAGENTA);
	init_pair(5, COLOR_BLACK, COLOR_CYAN);
	init_pair(6, COLOR_BLACK, COLOR_WHITE);

	mvprintw(10, 5, "Instructions:");
	mvprintw(11, 5, "Use the keys w, a, s, d");
	mvprintw(12, 5, "Press any key to start");

	wgetch(stdscr);

	make_thread(gameplay, NULL);
	make_thread(get_input, NULL);

	for (;;)
		sleep(2);
}
